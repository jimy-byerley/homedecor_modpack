local S = homedecor_i18n.gettext

-- vectors to place one node next to or behind another

homedecor.fdir_to_right = {
	{  1,  0 },
	{  0, -1 },
	{ -1,  0 },
	{  0,  1 },
}

homedecor.fdir_to_left = {
	{ -1,  0 },
	{  0,  1 },
	{  1,  0 },
	{  0, -1 },
}

homedecor.fdir_to_fwd = {
	{  0,  1 },
	{  1,  0 },
	{  0, -1 },
	{ -1,  0 },
}

-- special case for wallmounted nodes

homedecor.wall_fdir_to_right = {
	nil,
	nil,
	{ -1,  0 },
	{  1,  0 },
	{  0, -1 },
	{  0,  1 },
}

homedecor.wall_fdir_to_left = {
	nil,
	nil,
	{  1,  0 },
	{ -1,  0 },
	{  0,  1 },
	{  0, -1 },
}

homedecor.wall_fdir_to_fwd = {
	nil,
	nil,
	{  0, -1 },
	{  0,  1 },
	{  1,  0 },
	{ -1,  0 },
}

local placeholder_node = "air"
minetest.register_alias("homedecor:expansion_placeholder", "air")

--- select which node was pointed at based on it being known, not ignored, buildable_to
-- returns nil if no node could be selected
local function select_node(pointed_thing)
	local pos = pointed_thing.under
	local node = minetest.get_node_or_nil(pos)
	local def = node and minetest.registered_nodes[node.name]

	if not def or not def.buildable_to then
		pos = pointed_thing.above
		node = minetest.get_node_or_nil(pos)
		def = node and minetest.registered_nodes[node.name]
	end
	return def and pos, def
end

--- check if all nodes can and may be build to
local function is_buildable_to(placer_name, ...)
	for _, pos in ipairs({...}) do
		local node = minetest.get_node_or_nil(pos)
		local def = node and minetest.registered_nodes[node.name]
		if not (def and def.buildable_to) or minetest.is_protected(pos, placer_name) then
			return false
		end
	end
	return true
end

-- place one or two nodes if and only if both can be placed
local function stack(itemstack, placer, fdir, pos, def, pos2, node1, node2, pointed_thing)
	local placer_name = placer:get_player_name() or ""
	if is_buildable_to(placer_name, pos, pos2) then
		local lfdir = fdir or minetest.dir_to_facedir(placer:get_look_dir())
		minetest.set_node(pos, { name = node1, param2 = lfdir })
		node2 = node2 or "air" -- this can be used to clear buildable_to nodes even though we are using a multinode mesh
		-- do not assume by default, as we still might want to allow overlapping in some cases
		local has_facedir = node2 ~= "air"
		if node2 == "placeholder" then
			has_facedir = false
			node2 = placeholder_node
		end
		minetest.set_node(pos2, { name = node2, param2 = (has_facedir and lfdir) or nil })

		-- call after_place_node of the placed node if available
		local ctrl_node_def = minetest.registered_nodes[node1]
		if ctrl_node_def and ctrl_node_def.after_place_node then
			ctrl_node_def.after_place_node(pos, placer, itemstack, pointed_thing)
		end

		if not creative.is_enabled_for(placer_name) then
			itemstack:take_item()
		end
	end
	return itemstack
end

local function rightclick_pointed_thing(pos, placer, itemstack, pointed_thing)
	local node = minetest.get_node_or_nil(pos)
	if not node then return false end
	local def = minetest.registered_nodes[node.name]
	if not def or not def.on_rightclick then return false end
	return def.on_rightclick(pos, node, placer, itemstack, pointed_thing) or itemstack
end

-- Stack one node above another
-- leave the last argument nil if it's one 2m high node
function homedecor.stack_vertically(itemstack, placer, pointed_thing, node1, node2)
	local rightclick_result = rightclick_pointed_thing(pointed_thing.under, placer, itemstack, pointed_thing)
	if rightclick_result then return rightclick_result end

	local pos, def = select_node(pointed_thing)
	if not pos then return itemstack end

	local top_pos = { x=pos.x, y=pos.y+1, z=pos.z }

	return stack(itemstack, placer, nil, pos, def, top_pos, node1, node2, pointed_thing)
end

-- Stack one door node above another
-- like  homedecor.stack_vertically but tests first if it was placed as a right wing, then uses node1_right and node2_right instead

function homedecor.stack_wing(itemstack, placer, pointed_thing, node1, node2, node1_right, node2_right)
	local rightclick_result = rightclick_pointed_thing(pointed_thing.under, placer, itemstack, pointed_thing)
	if rightclick_result then return rightclick_result end

	local pos, def = select_node(pointed_thing)
	if not pos then return itemstack end

	local forceright = placer:get_player_control()["sneak"]
	local fdir = minetest.dir_to_facedir(placer:get_look_dir())

	local is_right_wing = node1 == minetest.get_node({ x = pos.x + homedecor.fdir_to_left[fdir+1][1], y=pos.y, z = pos.z + homedecor.fdir_to_left[fdir+1][2] }).name
	if forceright or is_right_wing then
		node1, node2 = node1_right, node2_right
	end

	local top_pos = { x=pos.x, y=pos.y+1, z=pos.z }
	return stack(itemstack, placer, fdir, pos, def, top_pos, node1, node2, pointed_thing)
end

function homedecor.stack_sideways(itemstack, placer, pointed_thing, node1, node2, dir)
	local rightclick_result = rightclick_pointed_thing(pointed_thing.under, placer, itemstack, pointed_thing)
	if rightclick_result then return rightclick_result end

	local pos, def = select_node(pointed_thing)
	if not pos then return itemstack end

	local fdir = minetest.dir_to_facedir(placer:get_look_dir())
	local fdir_transform = dir and homedecor.fdir_to_right or homedecor.fdir_to_fwd

	local pos2 = { x = pos.x + fdir_transform[fdir+1][1], y=pos.y, z = pos.z + fdir_transform[fdir+1][2] }

	return stack(itemstack, placer, fdir, pos, def, pos2, node1, node2, pointed_thing)
end

function homedecor.bed_expansion(pos, placer, itemstack, pointed_thing, trybunks)

	local thisnode = minetest.get_node(pos)
	local param2 = thisnode.param2
	local fdir = param2 % 8

	local fxd = homedecor.wall_fdir_to_fwd[fdir+1][1]
	local fzd = homedecor.wall_fdir_to_fwd[fdir+1][2]

	local forwardpos = {x=pos.x+fxd, y=pos.y, z=pos.z+fzd}
	local forwardnode = minetest.get_node(forwardpos)

	local def = minetest.registered_nodes[forwardnode.name]
	local placer_name = placer:get_player_name()

	if not (def and def.buildable_to) then
		minetest.chat_send_player( placer:get_player_name(),
				S("Not enough room - the space for the headboard is occupied!"))
		minetest.set_node(pos, {name = "air"})
		return true
	end

	if minetest.is_protected(forwardpos, placer_name) then
		minetest.chat_send_player( placer:get_player_name(),
				S("Someone already owns the spot where the headboard goes."))
		return true
	end

	minetest.set_node(forwardpos, {name = "air"})

	local lxd = homedecor.wall_fdir_to_left[fdir+1][1]
	local lzd = homedecor.wall_fdir_to_left[fdir+1][2]
	local leftpos = {x=pos.x+lxd, y=pos.y, z=pos.z+lzd}
	local leftnode = minetest.get_node(leftpos)

	local rxd = homedecor.wall_fdir_to_right[fdir+1][1]
	local rzd = homedecor.wall_fdir_to_right[fdir+1][2]
	local rightpos = {x=pos.x+rxd, y=pos.y, z=pos.z+rzd}
	local rightnode = minetest.get_node(rightpos)

	local inv = placer:get_inventory()

	if leftnode.name == "homedecor:bed_regular" then
		local newname = string.gsub(thisnode.name, "_regular", "_kingsize")
		local meta = minetest.get_meta(pos)
		local leftmeta = minetest.get_meta(leftpos)

		minetest.set_node(pos, {name = "air"})
		minetest.swap_node(leftpos, { name = newname, param2 = param2})
	elseif rightnode.name == "homedecor:bed_regular" then
		local newname = string.gsub(thisnode.name, "_regular", "_kingsize")
		local meta = minetest.get_meta(pos)
		local rightmeta = minetest.get_meta(rightpos)

		minetest.set_node(rightpos, {name = "air"})
		minetest.swap_node(pos, { name = newname, param2 = param2})
	end

	local toppos = {x=pos.x, y=pos.y+1.0, z=pos.z}
	local topposfwd = {x=toppos.x+fxd, y=toppos.y, z=toppos.z+fzd}

	if trybunks and is_buildable_to(placer_name, toppos, topposfwd) then
		local newname = string.gsub(thisnode.name, "_regular", "_extended")
		local newparam2 = param2 % 8
		minetest.swap_node(toppos, { name = thisnode.name, param2 = param2})
		minetest.swap_node(pos, { name = newname, param2 = param2})
		itemstack:take_item()
	end
end

function homedecor.unextend_bed(pos)
	local bottomnode = minetest.get_node({x=pos.x, y=pos.y-1.0, z=pos.z})
	local param2 = bottomnode.param2
	if bottomnode.name == "homedecor:bed_extended" then
		local newname = string.gsub(bottomnode.name, "_extended", "_regular")
		minetest.swap_node({x=pos.x, y=pos.y-1.0, z=pos.z}, { name = newname, param2 = param2})
	end
end

function homedecor.place_banister(itemstack, placer, pointed_thing)
	-- placement params
	local pos = pointed_thing.above
	local dir = placer:get_look_dir()
	local placedir = minetest.dir_to_facedir(dir)
	
	local vertical_dir = 'horizontal'
	local stair = minetest.get_node({x=pos.x+dir.x*1.5, y=pos.y, z=pos.z+dir.z*1.5})
	local def = minetest.registered_nodes[stair.name]
	local fdir = stair.param2 or 0
	
	local vertical_dir = '_horizontal'
	local horizontal_dir = ''
	-- if the node on top of which we build is a stair block
	if def.groups.stair then
		vertical_dir = '_diagonal'
		
		-- if stair direction is orthogonal to placement (so placer) direction
		if (fdir + placedir) % 2 == 1 then
			if fdir == (placedir+1)%4 then  -- determines on which side is the stair direction in the placer's view
				horizontal_dir = '_right'
				fdir = (fdir+3)%4	-- addapt fdir to model orientation
			else	
				horizontal_dir = '_left'
				fdir = (fdir+1)%4	-- addapt fdir to model orientation
			end
		else
			vertical_dir = '_horizontal'
		end
	end
	if vertical_dir == '_horizontal' then
		fdir = placedir
	end
	
	-- place new block
	local new_place_name = string.gsub(itemstack:get_name(), "_horizontal", vertical_dir..horizontal_dir)
	minetest.set_node(pointed_thing.above, {name = new_place_name, param2 = fdir})
	if not creative.is_enabled_for(placer.name) then itemstack:take_item() end
	return itemstack
end
